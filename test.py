from flask_cors import CORS, cross_origin
from flask import Flask

app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/add',methods=['GET','POST'])
@cross_origin(origins='*')
def add():
    acc = 'aaa'
    return 'add'

@app.route('/muti',methods=['GET'])
def muti():
    return 'muti'

