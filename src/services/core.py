from src.bases.service import Service

from config import CORE_URL, APP_ID


class CoreService(Service):
    base_url = CORE_URL

    def list_apps(self,
                  search_text: list = None,
                  status: list = None,
                  version: list = None,
                  created_before: list = None,
                  created_after: list = None,
                  page: int = None,
                  per_page: int = None,
                  ):

        payload = dict(
            page=page,
            per_page=per_page
        )

        if search_text:
            payload['search_text'] = ','.join(search_text)

        if status:
            payload['status'] = ','.join(status)

        if version:
            payload['version'] = ','.join(version)

        if created_before:
            payload['created_before'] = ','.join(
                list(map(lambda dt: dt.isoformat(), created_before))
            )

        if created_after:
            payload['created_after'] = ','.join(
                list(map(lambda dt: dt.isoformat(), created_after))
            )

        return self.do_request(
            method='GET',
            endpoint='/apps/list',
            payload=payload
        )

    def list_app_versions(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/apps/versions/list',
            payload=payload
        )

    def list_products(self,
                      **payload
                      ):

        return self.do_request(
            method='GET',
            endpoint='/products/list',
            payload=payload
        )

    def list_gateways(self,
                      **payload
                      ):

        return self.do_request(
            method='GET',
            endpoint='/gateways/list',
            payload=payload
        )

    def get_app(self, id: str):
        return self.do_request(
            method='GET',
            endpoint='/apps',
            payload={'id': id}
        )

    def update_app(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/apps',
            payload=payload
        )

    def list_docker_images(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/docker-images/list',
            payload=payload
        )

    def create_app_version(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/apps/versions',
            payload=payload
        )

    def update_app_version(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/apps/versions',
            payload=payload
        )

    def get_app_version(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/apps/versions',
            payload=payload
        )

    def update_currency(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/currencies',
            payload=payload
        )

    def create_currency(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/currencies',
            payload=payload
        )

    def list_currencies(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/currencies/list',
            payload=payload
        )

    def delete_currencies(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/currencies',
            payload=payload
        )

    def update_currency_conversion(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/currencies/conversions',
            payload=payload
        )

    def create_currency_conversion(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/currencies/conversions',
            payload=payload
        )

    def list_currency_conversions(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/currencies/conversions',
            payload=payload
        )

    def delete_currency_conversions(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/currencies/conversions',
            payload=payload
        )

    def create_gateway(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/gateways',
            payload=payload
        )

    def update_gateway(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/gateways',
            payload=payload
        )

    def delete_gateways(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/gateways',
            payload=payload
        )

    def list_languages(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/languages/list',
            payload=payload
        )

    def create_language(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/languages',
            payload=payload
        )

    def update_language(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/languages',
            payload=payload
        )

    def get_language(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/languages',
            payload=payload
        )

    def delete_language(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/languages',
            payload=payload
        )

    def list_time_formats(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/time-formats/list',
            payload=payload
        )

    def list_date_formats(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/date-formats/list',
            payload=payload
        )

    def list_app_products(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/apps/products/list',
            payload=payload
        )

    def create_app_product(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/apps/products',
            payload=payload
        )

    def create_app_domain(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/apps/domains',
            payload=payload
        )

    def delete_app_domain(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/apps/domains',
            payload=payload
        )

    def list_app_domains(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/apps/domains',
            payload=payload
        )

    def list_smtp(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/smtp',
            payload=payload
        )

    def create_smtp(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/smtp',
            payload=payload
        )

    def update_smtp(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/smtp',
            payload=payload
        )

    def delete_smtp(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/smtp',
            payload=payload
        )

    def list_traveler_fields(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/traveler-fields/list',
            payload=payload
        )

    def get_traveler_field(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/traveler-fields',
            payload=payload
        )

    def create_traveler_field(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/traveler-fields',
            payload=payload
        )

    def update_traveler_field(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/traveler-fields',
            payload=payload
        )

    def delete_traveler_fields(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/traveler-fields',
            payload=payload
        )

    def list_markups(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/markups',
            payload=payload
        )

    def create_markup(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/markups',
            payload=payload
        )

    def update_markup(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/markups',
            payload=payload
        )

    def delete_markups(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/markups',
            payload=payload
        )

    def list_providers(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/providers',
            payload=payload
        )

    def list_b2b_agents(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/b2b/agents',
            payload=payload
        )

    def create_b2b_agent(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/b2b/agents',
            payload=payload
        )

    def update_b2b_agent(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/b2b/agents',
            payload=payload
        )

    def delete_b2b_agents(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/b2b/agents',
            payload=payload
        )

    def list_b2b_agent_types(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/b2b/agent-types',
            payload=payload
        )

    def create_b2b_agent_type(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/b2b/agent-types',
            payload=payload
        )

    def update_b2b_agent_type(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/b2b/agent-types',
            payload=payload
        )

    def delete_b2b_agent_types(self, **payload):
        return self.do_request(
            method='DELETE',
            endpoint='/b2b/agent-types',
            payload=payload
        )

    def create_file(self, file, **payload):
        return self.do_request(
            method='POST',
            endpoint='/files',
            files={'file': (file.filename, file.read(), file.content_type)},
            payload=payload
        )

    def list_files(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/files',
            payload=payload
        )

    def list_bookings(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/bookings/list',
            payload=payload
        )

    def get_booking(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/bookings',
            payload=payload
        )

    def update_booking(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/bookings',
            payload=payload
        )

    def create_booking(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/bookings',
            payload=payload
        )

    def list_booking_products(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/bookings/products',
            payload=payload
        )

    def list_booking_travelers(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/bookings/travelers',
            payload=payload
        )

    def update_booking_traveler(self, **payload):
        return self.do_request(
            method='PATCH',
            endpoint='/bookings/travelers',
            payload=payload
        )

    def list_booking_prices(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/bookings/prices',
            payload=payload
        )

    def list_payments(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/payments/list',
            payload=payload
        )

    def get_payment(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/payments',
            payload=payload
        )

    def list_cms_nodes(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/cms/nodes',
            payload=payload
        )

    def upsert_cms_nodes(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/cms/nodes',
            payload=payload
        )

    def init_cms_node(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/cms/nodes/init',
            payload=payload
        )

    def list_cms_node_types(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/cms/node-types',
            payload=payload
        )

    def list_cms_templates(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/cms/templates',
            payload=payload
        )

    def list_app_processes(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/apps/deploy',
            payload=payload
        )

    def deploy_app(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/apps/deploy',
            payload=payload
        )

    def sync_app(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/apps/sync',
            payload=payload
        )

    def list_processes(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/processes',
            payload=payload
        )

    def create_process(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/processes',
            payload=payload
        )

    def list_balance_adjustments(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/balances/adjustments',
            payload=payload
        )

    def init_prices(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/init-prices',
            payload=payload
        )

    def init_payment(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/payments/init',
            payload=payload
        )

    def list_payment_methods(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/payments/options',
            payload=payload
        )

    def send_booking_mail(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/bookings/send-mail',
            payload=payload
        )

    def create_logging(self, **payload):
        return self.do_request(
            method='POST',
            endpoint='/loggings',
            payload=payload
        )

    def get_loggings(self, **payload):
        return self.do_request(
            method='GET',
            endpoint='/loggings',
            payload=payload
        )
