STRING_LENGTH = {
    'UUID4': 36,
    'EX_SHORT': 50,
    'SHORT': 100,
    'MEDIUM': 500,
    'LONG': 1000,
    'EX_LONG': 5000,
    'LARGE': 10000,
    'EX_LARGE': 100000
}

STRING_LENGTH_VALIDATORS = {
    'EX_SHORT': lambda value: len(value) <= STRING_LENGTH['EX_SHORT'],
    'SHORT': lambda value: len(value) <= STRING_LENGTH['SHORT'],
    'MEDIUM': lambda value: len(value) <= STRING_LENGTH['MEDIUM'],
    'LONG': lambda value: len(value) <= STRING_LENGTH['LONG'],
    'EX_LONG': lambda value: len(value) <= STRING_LENGTH['EX_LONG'],
    'LARGE': lambda value: len(value) <= STRING_LENGTH['LARGE'],
    'EX_LARGE': lambda value: len(value) <= STRING_LENGTH['EX_LARGE']
}

PAGINATION = {
    'page': 1,
    'per_page': 50
}

DICT_KEY_SEPARATOR = '~|~'

PHONE_REGEX = r'^(\+8[0-9]{9,12})$|^(0[0-9]{6,15})$'

HTTP_METHODS = [
    'GET',
    'PUT',
    'PATCH',
    'POST',
    'DELETE'
]