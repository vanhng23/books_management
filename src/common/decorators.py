import time

from functools import wraps
from requests.exceptions import ConnectionError, ConnectTimeout


def request_connection_handler(max_retry=2):
    def decorator(func):
        @wraps(func)
        def handle(*args, **kwargs):
            retry_count = 0
            error = None
            # retry if connection error happens
            while retry_count < max_retry:
                try:
                    response = func(*args, **kwargs)
                    return response

                except (ConnectTimeout,
                        ConnectionError) as e:
                    error = e
                    retry_count += 1
                    time.sleep(retry_count)
            raise error
        return handle
    return decorator

