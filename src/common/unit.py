import json
from datetime import datetime, timedelta

def get_datetime_now(timestamp=False, add_time=None):
    result = datetime.utcnow()
    if timestamp:
        return result.timestamp()

    if add_time:
        result = result + timedelta(seconds=add_time)

    return result

class CustomJsonEncoder(json.JSONEncoder):
    def default(self, o: any) -> any:
        if isinstance(o, datetime):
            return o.isoformat()
        return super(CustomJsonEncoder, self).default(o)
