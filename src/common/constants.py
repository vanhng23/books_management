import os

import config

ROOT_DIR = os.path.dirname(config.__file__)

TMP_DIR = os.path.join(ROOT_DIR, 'tmp')
if not os.path.exists(TMP_DIR):
    os.makedirs(TMP_DIR)

if 'local' in config.API_DOMAIN:
    API_URL = f'http://{config.API_DOMAIN}:5000'
else:
    API_URL = f'https://{config.API_DOMAIN}'

STRING_LENGTH = {
    'UUID4': 36,
    'EX_SHORT': 50,
    'SHORT': 100,
    'MEDIUM': 500,
    'LONG': 2000,
    'EX_LONG': 10000,
    'LARGE': 30000,
    'EX_LARGE': 200000
}

DATE_FORMAT = '%Y-%m-%d'
ISO_DATE_FORMAT = '%Y-%m-%d'
ISO_DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S'
ISO_FORMAT = '%Y-%m-%dT%H:%M:%S'

VALID_DATETIME_FORMATS = [
    ISO_FORMAT,
    ISO_DATE_FORMAT,
    '%Y-%m-%dT%H:%M:%SZ',
    '%Y-%m-%dT%H:%M:%S+00:00',
    '%Y-%m-%dT%H:%M:%S.%fZ',
    '%Y-%m-%dT%H:%M:%S.%f',
    '%Y-%m-%dT%H:%M:%S',
    '%Y-%m-%dT%H:%M',
    '%Y-%m-%d %H:%M',
    '%d/%m/%Y %H:%M',
    '%d/%m/%Y',
    '%Y-%m-%d',
]

PAGINATION = {
    'page': 1,
    'per_page': 50
}

PHONE_REGEX = r'^(\+8[0-9]{9,12})$|^(0[0-9]{6,15})$'

USER_STATUSES = [
    dict(id='active'),
    dict(id='locked')
]

DEFAULT_USER_STATUS = 'active'
PRICE_DISPLAY_DEFAULT = 'person'
ADULT_AGE_DEFAULT = 13
CHILD_AGE_DEFAULT = 3
INFANT_AGE_DEFAULT = 0
EARTH_KM_RADIUS = 6373

DICT_KEY_SEPARATOR = '~|~'
HTTP_METHODS = [
    'GET',
    'PUT',
    'PATCH',
    'POST',
    'DELETE'
]

MAILING_ADDRESS = 'noreply@easygds.com'

WEEKDAYS = [
    dict(
        id='mon',
        name='Mon',
        order=0
    ),
    dict(
        id='tue',
        name='Tue',
        order=1
    ),
    dict(
        id='wed',
        name='Wed',
        order=2
    ),
    dict(
        id='thu',
        name='Thu',
        order=3
    ),
    dict(
        id='fri',
        name='Fri',
        order=4
    ),
    dict(
        id='sat',
        name='Sat',
        order=5
    ),
    dict(
        id='sun',
        name='Sun',
        order=6
    )
]

AIRLINE_LOGO_BASE_URL = 'https://gqcdn.s3.amazonaws.com/airline-logos/svg'

TOUR_PROVIDERS = [
    # "GTA",
    # "HMS",
    # "HotelBeds",
    # "BeMyGuest",
    "Viator",
    # "Miki",
    # "Livn"
]
TOUR_PROVIDERS_GTA = "GTA"
TOUR_PROVIDERS_HMS = "HMS"
TOUR_PROVIDERS_HOTELBEDS = "HotelBeds"
TOUR_PROVIDERS_BEMYGUEST = "BeMyGuest"
TOUR_PROVIDERS_VIATOR = "Viator"
TOUR_PROVIDERS_VIATOR_V2 = "ViatorV2"
TOUR_PROVIDERS_MIKI = "Miki"
TOUR_PROVIDERS_LIVN = "Livn"

TOUR_PROVIDERS_USED = [TOUR_PROVIDERS_VIATOR_V2, TOUR_PROVIDERS_HMS]

FLIGHT_BOOKING_CLASSES = [
    dict(
        name='First Class',
        code='first_class',
        values=[
            'A', 'F'
        ]
    ),
    dict(
        name='Business Class',
        code='business_class',
        values=[
            'C', 'D', 'I', 'J'
        ]
    ),
    dict(
        name='Premium Economy Class',
        code='premium_economy_class',
        values=[
            'W', 'R', 'E'
        ]
    ),
    dict(
        name='Full Fare Economy Class',
        code='full_fare_economy_class',
        values=[
            'Y', 'B'
        ]
    ),
    dict(
        name='Economy Class',
        code='economy_class',
        values=[
            'H'
        ]
    ),
    dict(
        name='No Mileage Credit',
        code='no_mileage_credit',
        values=[
            'K', 'M', 'L', 'V', 'Q', 'S', 'G', 'N'
        ]
    ),
]

MAPPING_QUESTION_IDS = {
    'FULL_NAMES_FIRST': 'first_name',
    'FULL_NAMES_LAST': 'last_name',
    'DATE_OF_BIRTH': 'birthday',
    'AGEBAND': 'age',
    'PASSPORT_EXPIRY': 'passport_expire',
    'PASSPORT_NATIONALITY': 'passport_country',
    'PASSPORT_PASSPORT_NO': 'passport_number',
    # 'HEIGHT': '',
    'NUMBER_AND_UNIT': 'phone',
}

CACHE_TTL = 60 * 30

