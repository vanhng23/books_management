import inspect
import importlib
from flask import (request, current_app as app,
                   g)
from flask.views import View

from src.common.constants import HTTP_METHODS
from src.bases.error.api import *

from .method_handler import MethodHandler
from .auth_handler import BaseAuthenticationHandler


class MetaResource(type):
    def __init__(cls, name, bases, attrs):
        super(MetaResource, cls).__init__(name, bases, attrs)
        # the 'methods' module must be defined in the resource directory,
        # all the method handler classes are defined in this module
        methods_module_path = cls.__module__ + '.methods'
        try:
            methods_module = importlib.import_module(methods_module_path)
        except ModuleNotFoundError as e:
            print(methods_module_path, str(e))
            methods_module = None
        if not methods_module:
            return

        method_handlers = attrs.get('method_handlers', {})

        classes = inspect.getmembers(methods_module,
                                     inspect.isclass)

        for cls_name, _class in classes:
            if not issubclass(_class, MethodHandler):
                continue
            if cls_name.upper() not in HTTP_METHODS:
                continue
            method_handlers[cls_name.upper()] = _class

        cls.method_handlers = method_handlers


class Resource(View, metaclass=MetaResource):
    app = None
    request = None
    mongo_db = None
    session = None
    access_token = None

    meta = {}

    # all available methods
    methods = HTTP_METHODS

    # the endpoint of the resource,
    # if this attr is None then this resource
    # will never be registered to the api
    endpoint = None

    # for toggling resource authentication
    auth_required = False

    authentication_handler_class = BaseAuthenticationHandler

    # a dict contains the method handlers of this resource
    method_handlers = {}

    def __init__(self):
        self.app = app
        self.request = request
        self.mongo_db = g.mongo_db
        self.session = g.sql_session

    def _before_auth(self):
        pass

    def _after_auth(self):
        pass

    def _init_method_handler(self, handler_class):
        result = handler_class(
            access_token=self.access_token,
            request=self.request,
            mongo_db=self.mongo_db,
            session=self.session,
            app=self.app,
        )
        return result

    def dispatch_request(self, *args, **kwargs):
        """The main flow of an api resource"""
        # check method available
        method_handler_class = self.method_handlers.get(request.method.upper())
        if not method_handler_class:
            raise MethodNotAllowed

        self._before_auth()

        # check authentication
        authentication_handler = self.authentication_handler_class(
            mongo_db=g.mongo_db,
            secret_key=app.config['SECRET_KEY'],
            request=request
        )
        auth_data = authentication_handler.run()
        if auth_data:
            access_token = auth_data
        else:
            access_token  = None

        self.access_token = access_token

        if not access_token and self.auth_required:
            raise Unauthorized

        self._after_auth()

        # handle method
        method_handler = self._init_method_handler(
            handler_class=method_handler_class
        )

        return method_handler.run()


__all__ = (
    'Resource'
)
