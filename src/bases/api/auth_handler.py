from src.common.utils import decode_jwt_token, log


class AuthenticationHandler(object):
    def __init__(self, request, mongo_db, secret_key):
        self.request = request
        self.mongo_db = mongo_db
        self.secret_key = secret_key

    def run(self):
        """Return User or None"""
        raise NotImplementedError


class BaseAuthenticationHandler(AuthenticationHandler):
    def run(self):
        auth_token = self.request.headers.get('Authorization', None)
        if not auth_token:
            return None

        try:
            bearer, access_token = auth_token.split(' ')
        except ValueError:
            return None

        if bearer != 'Bearer' or not access_token:
            return None

        return access_token

#
# class BaseAuthenticationAgentAccountHandler(AuthenticationHandler):
#
#     def run(self):
#         auth_data = self.request.headers.get('Authorization', None)
#         if not auth_data:
#             return None
#         try:
#             bearer, access_token = auth_data.split(' ')
#         except ValueError:
#             return None
#
#         if bearer != 'Bearer' or not access_token:
#             return None
#
#         token_data = decode_jwt_token(token=access_token,
#                                       secret_key=self.secret_key)
#         print('13', token_data)
#
#         if not token_data:
#             return None
#         print('1')
#
#         try:
#             user_id = token_data['id']
#         except Exception as error:
#             print(error)
#             return None
#         print('xxx')
#         user = token_data
#         print(token_data)
#         return user
