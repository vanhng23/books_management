import json
from flask import Response, make_response

from src.common.json_encoders import CustomJsonEncoder
from src.bases.schema import BaseSchema
from src.bases.service import ServiceError
from src.bases.error.api import (Unauthorized, BadRequestParams,
                                 PermissionError, ApiSessionError)


class PayloadFetcher(object):
    def __init__(self, request):
        self.request = request

    def fetch(self):
        try:
            if self.request.method in ['GET', 'DELETE']:
                payload = self.request.args or {}
            else:
                payload = self.request.json or {}
        except Exception:
            payload = {}
        return payload.copy()


class MethodHandler(object):
    # HTTP method authentication toggle
    auth_required = False

    payload_fetcher = PayloadFetcher

    permission_requirements = None

    input_schema_class = BaseSchema

    output_handler = None

    raw_params = None

    # request payload
    payload = None

    # indicate which keys will be returned in the result data,
    # empty means all keys.
    output_keys = []

    def __init__(self,
                 app,
                 request,
                 mongo_db,
                 session,
                 access_token,
                 **kwargs):
        self.app = app
        self.request = request
        self.mongo_db = mongo_db
        self.session = session
        self.access_token = access_token

        self._check_auth()

        self.raw_params = self._get_raw_params()
        self.output_keys = self._get_output_keys()

        self.payload = self._parse_raw_params()

    def _get_raw_params(self):
        handler = self.payload_fetcher(request=self.request)
        return handler.fetch()

    def _handle_api_logic(self):
        """The main logic of this HTTP method"""
        return dict(success=True)

    def _parse_raw_params(self):
        schema = self.input_schema_class(unknown='EXCLUDE')

        err = schema.validate(self.raw_params)
        if err:
            raise BadRequestParams(message=str(err))
        result = schema.load(self.raw_params)
        return result

    def _check_auth(self):
        if self.auth_required and not self.access_token:
            raise Unauthorized

    def _get_output_keys(self):
        return self.raw_params.get('output_keys') or []

    def run(self) -> Response:
        """The main flow of this HTTP method"""

        # handle logic
        result = self._handle_api_logic()

        response = make_response(json.dumps(result,
                                            cls=CustomJsonEncoder), 200)
        response.headers['Content-Type'] = 'application/json'

        return response


class BasePost(MethodHandler):
    def _get_raw_params(self):
        params = self.request.json

        if not params:
            return {}

        return params.copy()


class BasePatch(MethodHandler):
    def _get_raw_params(self):
        params = self.request.json
        if not params:
            return {}

        return params.copy()


class BasePut(MethodHandler):
    def _get_raw_params(self):
        params = self.request.json
        if not params:
            return {}

        return params.copy()


class BaseGet(MethodHandler):
    pass


class BaseDelete(MethodHandler):
    pass
