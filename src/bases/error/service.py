from src.bases.error import BaseError


class ServiceError(BaseError):
    pass
