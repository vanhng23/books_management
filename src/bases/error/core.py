from src.bases.error import BaseError


class CoreError(BaseError):
    pass
