import json
from pymongo import MongoClient

from config import APP_CODE, ENV
from .postgres import Postgres

class Mongo(object):
    def __init__(self, uri):
        self.client = MongoClient(uri)

    def get_database(self, *args, **kwargs):
        return self.client.get_database(f'{ENV}-{APP_CODE}', *args, **kwargs)


__all__ = (
    'Mongo',
    'Postgres'
)
