from operator import index
from xmlrpc.client import DateTime
from sqlalchemy import (Boolean, Integer, Column, String)
from src.base.data import BaseData
from sqlalchemy.dialects.postgresql import JSONB
from src.common.constant import STRING_LENGTH


class Books(BaseData):
    code = Column(String(STRING_LENGTH['UUID4']), nullable = False, index = True)
    title = Column(String(STRING_LENGTH['LONG']), nullable = False, index = True)
    description = Column(String(STRING_LENGTH['LONG']), nullable = False, index = True)
    image = Column(JSONB, default={})
    scope_id = Column(String(STRING_LENGTH['UUID4']), index = True)
    scope_type = Column(String(STRING_LENGTH['EX_SHORT']), index = True)

    rounding = Column(Integer, default = 2)

    is_default = Column(Boolean, default = False, index = True)
    enabled = Column(Boolean, default = True, index = True)