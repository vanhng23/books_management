from src.databases import Postgres, Mongo
from src.bases.api.factory import Factory
from src.api import resources

from config import (ApiConfig, POSTGRES_URI, MONGO_URI)


mongo = Mongo(MONGO_URI)
mongo_db = mongo.get_database()

sql_db = Postgres(POSTGRES_URI)
sql_session_factory = sql_db.create_session_factory(
    disable_autoflush=True
)

factory = Factory(
    config=ApiConfig,
    sql_session_factory=sql_session_factory,
    mongo_db=mongo_db,
    resource_module=resources
)

app = factory.create_app()
