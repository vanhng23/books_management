from src.bases.api.resource import Resource


class CurrencyListResource(Resource):
    endpoint = '/currencies/list'
