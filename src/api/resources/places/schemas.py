from pickle import FALSE
from src.bases.schema import (BaseListingSchema, fields, StringField,
                              ListField,
                              STRING_LENGTH_VALIDATORS, TimeField, RangeField,
                              IdField)


class GetSchema(BaseListingSchema):
    language_code = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'],
                                required=True)
    search_text = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'],
                              allow_none=True)
    ancestor_id = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'],
                              allow_none=True)
    association_id = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'],
                              allow_none=True)
    descendant_id = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'],
                              allow_none=True)

    codes = ListField(
        StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT']),
        allow_none=True)
    country_codes = ListField(
        StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT']),
        allow_none=True)
    types = ListField(
        StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT']),
        allow_none=True)
    ids = ListField(
        StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT']),
        allow_none=True)

    viator_id_exists = fields.Boolean(missing=False)
    expedia_id_exists = fields.Boolean(allow_none=True)
    property_included = fields.Boolean(allow_none=True)
    tour_included = fields.Boolean(allow_none=True)

    latitude = fields.Float(allow_none=True)
    longitude = fields.Float(allow_none=True)
