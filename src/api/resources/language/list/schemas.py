from pickle import FALSE
from src.bases.schema import (BaseListingSchema, fields, StringField,
                              ListField,
                              STRING_LENGTH_VALIDATORS, TimeField, RangeField,
                              IdField)


class GetSchema(BaseListingSchema):
    language_code = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'],
                                required=True)
