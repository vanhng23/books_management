from src.api.base_resource import BaseResource

class LanguageListResource(BaseResource):
    endpoint = '/language/list'
