from src.bases.api.method_handler import BaseGet


class Get(BaseGet):

    def _handle_api_logic(self):
        return dict(ok=self._db_check())

    def _db_check(self):
        result = True
        try:
            self.mongo_db.app.find_one()
        except Exception as e:
            result = False
        return result

