from sqlalchemy import or_
from model.passenger import Passenger
from src.base.method_handler import MethodHandler
from .schemas import GetSchema


class Get(MethodHandler):
    baseSchema = GetSchema

    def handle_logic(self):
        scope_type = self.payload.get('scope_type')
        scope_id = self.payload.get('scope_id')

        query = self.session.query(Passenger)
        if scope_type:
            query = query.filter(Passenger.scope_type.in_(scope_type))

        if scope_id:
            query = query.filter(Passenger.scope_id.in_(scope_id))
        
        total = query.count()

        result = []
        for i in query.sort(
            Passenger,
            *(self.payload.get('sorts') or [])
        ).paginate(
            page=self.payload.get('page'),
            per_page=self.payload.get('per_page')
        ):
            record = i.to_json(exclude_fields=['description'])
            result.append(record)

        return dict(
            total=total,
            result=result
        )