from src.common.constant import STRING_LENGTH_VALIDATORS
from src.base.schema import (IdField, BaseListSchema, fields,
                            StringField, ListField, DateTimeField)

class GetSchema(BaseListSchema):
    scope_id = ListField(IdField, allow_none=True)
    scope_type = ListField(StringField, allow_none=True)
    search_text = ListField(StringField, allow_none=True)
    created_before = ListField(DateTimeField, allow_none=True)
    created_after = ListField(DateTimeField, allow_none=True)