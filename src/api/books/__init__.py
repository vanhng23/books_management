from src.base.api.resource import Resource

class BooksResource(Resource):
    endpoint = '/books'