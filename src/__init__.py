import os
import logging
import click

from config import (POSTGRES_URI, MONGO_URI, ENV,
                    DATADOG_ENABLE, DATADOG_PORT, DATADOG_HOST)


def execute_command(command):
    import subprocess
    return subprocess.check_call(command)


def setup_logging():
    from src.common.utils import log

    ch = logging.StreamHandler()
    ch.setFormatter(logging.Formatter(
        "%(asctime)s --  %(levelname)s  -- %(message)s")
    )
    log.setLevel(logging.DEBUG)
    log.addHandler(ch)


def setup_datadog():
    from ddtrace import patch_all

    env_map = {
        'dev': 'holidays-dev',
        'staging': 'holidays-stg',
        'production': 'holidays-prd'
    }
    os.environ['DD_ENV'] = env_map[ENV]
    os.environ['DD_SERVICE'] = 'ota-holidays'
    os.environ['DD_AGENT_HOST'] = DATADOG_HOST
    os.environ['DD_AGENT_PORT'] = str(DATADOG_PORT)

    patch_all()


@click.group()
def cli():
    pass


@cli.command(short_help='Runs a shell in the app context.')
@click.argument('ipython_args', nargs=-1, type=click.UNPROCESSED)
def shell(ipython_args):
    import sys
    import IPython
    from IPython.terminal.ipapp import load_default_config

    from src.databases import Postgres, Mongo

    setup_logging()

    ip_config = load_default_config()

    postgres_db = Postgres(POSTGRES_URI)

    session_factory = postgres_db.create_session_factory(
        disable_autoflush=True)
    session = session_factory()

    mongo = Mongo(MONGO_URI)

    ctx = dict(
        session=session,
        session_factory=session_factory,
        mongo_db=mongo.get_database()
    )

    banner = 'Python %s on %s\n' % (sys.version, sys.platform)
    if ctx:
        banner += 'Objects created:'
    for k, v in ctx.items():
        banner += '\n    {0}: {1}'.format(k, v)
    ip_config.TerminalInteractiveShell.banner1 = banner
    IPython.start_ipython(argv=ipython_args, user_ns=ctx, config=ip_config)


@cli.command(short_help='Run an api.')
@click.option('--uwsgi', default='false')
@click.option('--port', default='5000')
@click.option('--processes', default='1')
@click.option('--threads', default='500')
@click.option('--buffer-size', default='65535')
@click.option('--host')
def api(**kwargs):
    if DATADOG_ENABLE:
        setup_datadog()

    setup_logging()

    uwsgi_enabled = False if kwargs['uwsgi'] == 'false' else True
    host = kwargs.get('host')
    try:
        port = int(kwargs['port'])
    except Exception as e:
        raise e

    if not uwsgi_enabled:
        from src.api import app
        params = dict(port=port)
        if host:
            params['host'] = host
        return app.run(**params)

    try:
        processes = int(kwargs['processes'])
        threads = int(kwargs['threads'])
        buffer_size = int(kwargs['buffer_size'])
    except Exception as e:
        raise e

    command = [
        'uwsgi',
        '--wsgi-file=src/api/__init__.py',
        '--processes=%s' % processes,
        '--threads=%s' % threads,
        '--buffer-size=%s' % buffer_size,
        '--lazy-apps',
        '--callable=app',
        '--enable-threads'
    ]

    if DATADOG_ENABLE:
        command.append('--import=ddtrace.bootstrap.sitecustomize')

    if host:
        command.append('--http-socket=%s:%s' % (host, port))
    else:
        command.append('--http-socket=:%s' % port)

    command.extend(['--lazy-apps', '--callable=app', '--enable-threads'])

    return execute_command(command)
