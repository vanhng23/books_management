from src.base.error.base import BaseError


class ServiceError(BaseError):
    pass
