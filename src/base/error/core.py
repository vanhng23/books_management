from src.base.error.base import BaseError


class CoreError(BaseError):
    pass
