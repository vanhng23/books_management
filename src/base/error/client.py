from src.base.error.base import BaseError


class ClientError(BaseError):
    pass
