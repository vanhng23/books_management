from unittest import result
from marshmallow import Schema, fields as marshmallow_fields, missing

from src.common.constant import PAGINATION, STRING_LENGTH_VALIDATORS
from .fields import *

class BaseSchema(Schema):
    pass

class BaseListSchema(BaseSchema):
    page = marshmallow_fields.Integer(missing=PAGINATION['page'])
    per_page = marshmallow_fields.Integer(missing=PAGINATION['per_page'])

    # sorts = ListField(StringField(validate=STRING_LENGTH_VALIDATORS['LONG']),default=['-created_at'])
    sorts = ListField(StringField(validate=STRING_LENGTH_VALIDATORS['LONG']))

    def load(self, data, **kwargs):
        result = super().load(data, **kwargs)
        if not result.get('page'):
            result['page']= PAGINATION['page']

        if not result.get('per_page'):
            result['per_page']= PAGINATION['per_page']

        if not result.get('sorts'):
            result['sorts']= ['-created_at']

        return result