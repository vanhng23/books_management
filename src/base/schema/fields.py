import re
import json

from marshmallow import fields, ValidationError
from datetime import datetime

from src.common.constant import STRING_LENGTH, PHONE_REGEX
from src.common.errorMessage import DATA_MESSAGE

class StringField(fields.String):
    def _serialize(self, value, attr=None, obj=None, **kwargs):
        val = super()._serialize(value, attr=None, obj=None, **kwargs)
        if val:
            val =  val.strip()        
        return val

class IdField(StringField):
    def _validate(self, value):
        if len(value) != STRING_LENGTH['UUID4']:
            raise ValidationError(DATA_MESSAGE['ID_INVALID'])

class DateTimeField(fields.DateTime):
    def _validate(self, value):
        if not isinstance(value, datetime):
            if isinstance(value, (float, int)):
                try:
                    value = datetime.fromtimestamp(value)
                except Exception:
                    raise ValidationError(DATA_MESSAGE['DATETIME_INVALID'])
        return super()._validate(value)
    
    def _deserialize(self, value, attr=None, data=None, **kwargs):
        if isinstance(value,(str, int)):
            try:
                value = float(value)
            except ValueError:
                pass

        if isinstance(value, str) and not value:
            if self.allow_none: 
                value = None
                
        if isinstance(value, float):
            value = datetime.fromtimestamp(value)

        return super()._deserialize(value, attr, data, **kwargs)

class ListField(fields.List):
    def _validate(self, value):
        if isinstance(value, str):
            if not value:
                value = []
            else:
                value = value.split(',')

        return super()._validate(value)

    def _deserialize(self, value, attr=None, data=None, **kwargs):
        if isinstance(value, str):
            if not value:
                value = []
            else:
                value = value.split(',')
        return super()._deserialize(value, attr, data, **kwargs)