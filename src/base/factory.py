from werkzeug.exceptions import HTTPException
from src.common.httpMessage import *
from flask import Flask,  current_app as app, request, make_response, jsonify, g
from flask_cors import CORS, cross_origin

class Factory(object):
    def __init__(self,
                 config,
                 sql_session_factory=None,
                 resource_module=None,
                 error_handler=None,
                 request_callback=None,
                 response_callback=None):
        self.config = config
        self.sql_session_factory = sql_session_factory
        self.resource_module = resource_module
        self.error_handler = error_handler
        self.request_callback = request_callback
        self.response_callback = response_callback

    @staticmethod
    def default_error_handler(app):
        @app.errorhandler(Exception)
        def handle_error(e):
            if isinstance(e, HTTPError):
                status_code = e.status_code
                data = e.output()
            elif isinstance(e, HTTPException):
                status_code = e.code
                data = e.__class__.__name__
            else:
                status_code = 500
                data = dict(message='Server error - %s' % e)

            if status_code >= 500:
                if app.debug:
                    raise e

            return make_response(jsonify(data), status_code)

    def create_app(self):
        app = Flask(__name__)

        '''Cross origin'''
        CORS(app, supports_credentials=True,
             automatic_options=True)

        '''Load config'''
        app.config.from_object(self.config)

        '''Error handling configuration'''
        error_handler = self.error_handler or self.default_error_handler
        error_handler(app)

        '''Callbacks configuration'''
        @app.before_request
        def handle_before_request():
            #self.log_request()
            g.sql_session = self.sql_session_factory()

        @app.after_request
        def handle_after_request(response):
            g.sql_session.close()
            self.sql_session_factory.remove()
            return response

        '''Resources installation'''
        #self.install_resource(app)

        return app