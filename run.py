from src.database import Postgres
from flask import  current_app as app, request, make_response, jsonify, g
from config import ApiConfig
import json
from src.base.factory import Factory

def shell(ipython_args):
    import sys
    import IPython
    from IPython.terminal.ipapp import load_default_config

    ip_config = load_default_config()

    postgres_db = Postgres('postgresql://postgres:Vanhng2304@localhost:5432/books_management')

    session_factory = postgres_db.create_session_factory(
        disable_autoflush=True)
    session = session_factory()

    ctx = dict(
        session=session,
        session_factory=session_factory
    )

    banner = 'Python %s on %s\n' % (sys.version, sys.platform)
    if ctx:
        banner += 'Objects created:'
    for k, v in ctx.items():
        banner += '\n    {0}: {1}'.format(k, v)
    ip_config.TerminalInteractiveShell.banner1 = banner
    IPython.start_ipython(argv=ipython_args, user_ns=ctx, config=ip_config)

def add():
    from src.api.books.methods import Get
    """The main flow of this HTTP method"""
    self = Get(request=request,
            session=g.sql_session,
            app=app)

    result = Get.handle_logic(self)
    response = make_response(json.dumps(result, cls=CustomJsonEncoder), 200)
    response.headers['Content-Type'] = 'application/json'

    return response

class CustomJsonEncoder(json.JSONEncoder):
    
    def default(self, o: any) -> any:
        from datetime import datetime
        if isinstance(o, datetime):
            return o.isoformat()
        return super(CustomJsonEncoder, self).default(o)

    

if __name__ == '__main__':

    sql_db = Postgres('postgresql://postgres:Vanhng2304@localhost:5432/books_management')
    sql_session_factory = sql_db.create_session_factory(
        disable_autoflush=True
    )

    factory = Factory(
        config=ApiConfig,
        sql_session_factory=sql_session_factory,
    )

    app = factory.create_app()
    app.add_url_rule('/add',view_func=add)

    app.run(debug=True, host='0.0.0.0', port=5000)
