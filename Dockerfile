FROM python:3.8.10

RUN pip install poetry

WORKDIR /app

COPY . /app

RUN poetry install --no-interaction

CMD ['./docker-entrypoint.sh']
